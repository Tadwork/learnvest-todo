package com.learnvest.todo.web.rest;

import com.learnvest.todo.Application;
import com.learnvest.todo.domain.TodoItem;
import com.learnvest.todo.repository.TodoItemRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the TodoItemResource REST controller.
 *
 * @see TodoItemResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class TodoItemResourceIntTest {

    private static final String DEFAULT_CONTENT = "AAAAA";
    private static final String UPDATED_CONTENT = "BBBBB";

    private static final Boolean DEFAULT_COMPLETED = false;
    private static final Boolean UPDATED_COMPLETED = true;

    private static final LocalDate DEFAULT_DUE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DUE_DATE = LocalDate.now(ZoneId.systemDefault());

    @Inject
    private TodoItemRepository todoItemRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restTodoItemMockMvc;

    private TodoItem todoItem;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TodoItemResource todoItemResource = new TodoItemResource();
        ReflectionTestUtils.setField(todoItemResource, "todoItemRepository", todoItemRepository);
        this.restTodoItemMockMvc = MockMvcBuilders.standaloneSetup(todoItemResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        todoItem = new TodoItem();
        todoItem.setContent(DEFAULT_CONTENT);
        todoItem.setCompleted(DEFAULT_COMPLETED);
        todoItem.setDueDate(DEFAULT_DUE_DATE);
    }

    @Test
    @Transactional
    public void createTodoItem() throws Exception {
        int databaseSizeBeforeCreate = todoItemRepository.findAll().size();

        // Create the TodoItem

        restTodoItemMockMvc.perform(post("/api/todoItems")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(todoItem)))
                .andExpect(status().isCreated());

        // Validate the TodoItem in the database
        List<TodoItem> todoItems = todoItemRepository.findAll();
        assertThat(todoItems).hasSize(databaseSizeBeforeCreate + 1);
        TodoItem testTodoItem = todoItems.get(todoItems.size() - 1);
        assertThat(testTodoItem.getContent()).isEqualTo(DEFAULT_CONTENT);
        assertThat(testTodoItem.getCompleted()).isEqualTo(DEFAULT_COMPLETED);
        assertThat(testTodoItem.getDueDate()).isEqualTo(DEFAULT_DUE_DATE);
    }

    @Test
    @Transactional
    public void getAllTodoItems() throws Exception {
        // Initialize the database
        todoItemRepository.saveAndFlush(todoItem);

        // Get all the todoItems
        restTodoItemMockMvc.perform(get("/api/todoItems?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(todoItem.getId().intValue())))
                .andExpect(jsonPath("$.[*].content").value(hasItem(DEFAULT_CONTENT.toString())))
                .andExpect(jsonPath("$.[*].completed").value(hasItem(DEFAULT_COMPLETED.booleanValue())))
                .andExpect(jsonPath("$.[*].dueDate").value(hasItem(DEFAULT_DUE_DATE.toString())));
    }

    @Test
    @Transactional
    public void getTodoItem() throws Exception {
        // Initialize the database
        todoItemRepository.saveAndFlush(todoItem);

        // Get the todoItem
        restTodoItemMockMvc.perform(get("/api/todoItems/{id}", todoItem.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(todoItem.getId().intValue()))
            .andExpect(jsonPath("$.content").value(DEFAULT_CONTENT.toString()))
            .andExpect(jsonPath("$.completed").value(DEFAULT_COMPLETED.booleanValue()))
            .andExpect(jsonPath("$.dueDate").value(DEFAULT_DUE_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTodoItem() throws Exception {
        // Get the todoItem
        restTodoItemMockMvc.perform(get("/api/todoItems/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTodoItem() throws Exception {
        // Initialize the database
        todoItemRepository.saveAndFlush(todoItem);

		int databaseSizeBeforeUpdate = todoItemRepository.findAll().size();

        // Update the todoItem
        todoItem.setContent(UPDATED_CONTENT);
        todoItem.setCompleted(UPDATED_COMPLETED);
        todoItem.setDueDate(UPDATED_DUE_DATE);

        restTodoItemMockMvc.perform(put("/api/todoItems")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(todoItem)))
                .andExpect(status().isOk());

        // Validate the TodoItem in the database
        List<TodoItem> todoItems = todoItemRepository.findAll();
        assertThat(todoItems).hasSize(databaseSizeBeforeUpdate);
        TodoItem testTodoItem = todoItems.get(todoItems.size() - 1);
        assertThat(testTodoItem.getContent()).isEqualTo(UPDATED_CONTENT);
        assertThat(testTodoItem.getCompleted()).isEqualTo(UPDATED_COMPLETED);
        assertThat(testTodoItem.getDueDate()).isEqualTo(UPDATED_DUE_DATE);
    }

    @Test
    @Transactional
    public void deleteTodoItem() throws Exception {
        // Initialize the database
        todoItemRepository.saveAndFlush(todoItem);

		int databaseSizeBeforeDelete = todoItemRepository.findAll().size();

        // Get the todoItem
        restTodoItemMockMvc.perform(delete("/api/todoItems/{id}", todoItem.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<TodoItem> todoItems = todoItemRepository.findAll();
        assertThat(todoItems).hasSize(databaseSizeBeforeDelete - 1);
    }
}
