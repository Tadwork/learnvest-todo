package com.learnvest.todo.web.rest;

import com.learnvest.todo.Application;
import com.learnvest.todo.domain.TodoList;
import com.learnvest.todo.repository.TodoListRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the TodoListResource REST controller.
 *
 * @see TodoListResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class TodoListResourceIntTest {

    private static final String DEFAULT_SUBJECT = "AAAAA";
    private static final String UPDATED_SUBJECT = "BBBBB";

    @Inject
    private TodoListRepository todoListRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restTodoListMockMvc;

    private TodoList todoList;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TodoListResource todoListResource = new TodoListResource();
        ReflectionTestUtils.setField(todoListResource, "todoListRepository", todoListRepository);
        this.restTodoListMockMvc = MockMvcBuilders.standaloneSetup(todoListResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        todoList = new TodoList();
        todoList.setSubject(DEFAULT_SUBJECT);
    }

    @Test
    @Transactional
    public void createTodoList() throws Exception {
        int databaseSizeBeforeCreate = todoListRepository.findAll().size();

        // Create the TodoList

        restTodoListMockMvc.perform(post("/api/todoLists")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(todoList)))
                .andExpect(status().isCreated());

        // Validate the TodoList in the database
        List<TodoList> todoLists = todoListRepository.findAll();
        assertThat(todoLists).hasSize(databaseSizeBeforeCreate + 1);
        TodoList testTodoList = todoLists.get(todoLists.size() - 1);
        assertThat(testTodoList.getSubject()).isEqualTo(DEFAULT_SUBJECT);
    }
   // http://stackoverflow.com/questions/30455362/jhipster-jparepository-principal-username-query-org-springframework-expre
    @Test
    @Transactional
    public void getAllTodoLists() throws Exception {
        // Initialize the database
        todoListRepository.saveAndFlush(todoList);

        // Get all the todoLists
        restTodoListMockMvc.perform(get("/api/todoLists?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(todoList.getId().intValue())))
                .andExpect(jsonPath("$.[*].subject").value(hasItem(DEFAULT_SUBJECT.toString())));
    }

    @Test
    @Transactional
    public void getTodoList() throws Exception {
        // Initialize the database
        todoListRepository.saveAndFlush(todoList);

        // Get the todoList
        restTodoListMockMvc.perform(get("/api/todoLists/{id}", todoList.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(todoList.getId().intValue()))
            .andExpect(jsonPath("$.subject").value(DEFAULT_SUBJECT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTodoList() throws Exception {
        // Get the todoList
        restTodoListMockMvc.perform(get("/api/todoLists/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTodoList() throws Exception {
        // Initialize the database
        todoListRepository.saveAndFlush(todoList);

		int databaseSizeBeforeUpdate = todoListRepository.findAll().size();

        // Update the todoList
        todoList.setSubject(UPDATED_SUBJECT);

        restTodoListMockMvc.perform(put("/api/todoLists")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(todoList)))
                .andExpect(status().isOk());

        // Validate the TodoList in the database
        List<TodoList> todoLists = todoListRepository.findAll();
        assertThat(todoLists).hasSize(databaseSizeBeforeUpdate);
        TodoList testTodoList = todoLists.get(todoLists.size() - 1);
        assertThat(testTodoList.getSubject()).isEqualTo(UPDATED_SUBJECT);
    }

    @Test
    @Transactional
    public void deleteTodoList() throws Exception {
        // Initialize the database
        todoListRepository.saveAndFlush(todoList);

		int databaseSizeBeforeDelete = todoListRepository.findAll().size();

        // Get the todoList
        restTodoListMockMvc.perform(delete("/api/todoLists/{id}", todoList.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<TodoList> todoLists = todoListRepository.findAll();
        assertThat(todoLists).hasSize(databaseSizeBeforeDelete - 1);
    }
}
