'use strict';

describe('Directive Tests', function () {

beforeEach(module("my.templates"));
    beforeEach(mockApiAccountCall);
//    beforeEach(mockI18nCalls);
    var elm, scope, $httpBackend;

    beforeEach(inject(function($compile, $rootScope, $injector) {
        $httpBackend = $injector.get('$httpBackend');

        var html = '<todo-item item="item"></todo-item>';
        scope = $rootScope.$new();
        elm = angular.element(html);
        $compile(elm)(scope);
    }));

    describe('Todo Item', function () {
        it("Should have an input", function() {
            expect(elm.find('input').length).toEqual(3);
        });

        it("the text should be the content,be complete, and due", function() {
            var due = new Date()
            scope.$apply(function() {
                scope.item = {
                "content":"test",
                "completed":true,
                "dueDate": due
                };
            });
            var text = elm.find('input[type=checkbox]').val();
            expect(text).toBe('');
            var text = elm.find('input[type=text]').val();
            expect(text).toContain('test');
        });

/*
        it("Should change the first 4 points of the strength bar", function() {
            scope.$apply(function() {
                scope.password = "mo5ch$=!"; // that should trigger the 3 first points
            });

            var firstpointStyle = elm.find('ul').children('li')[0].getAttribute('style');
            dump(firstpointStyle);
            expect(firstpointStyle).toContain('background-color: rgb(153, 255, 0)');

            var secondpointStyle = elm.find('ul').children('li')[1].getAttribute('style');
            expect(secondpointStyle).toContain('background-color: rgb(153, 255, 0)');

            var thirdpointStyle = elm.find('ul').children('li')[2].getAttribute('style');
            expect(thirdpointStyle).toContain('background-color: rgb(153, 255, 0)');

            var fourthpointStyle = elm.find('ul').children('li')[3].getAttribute('style');
            expect(fourthpointStyle).toContain('background-color: rgb(153, 255, 0)');

            var fifthpointStyle = elm.find('ul').children('li')[4].getAttribute('style');
            expect(fifthpointStyle).toContain('background-color: rgb(221, 221, 221)');
        });*/
    });
});
