'use strict';

angular.module('todoApp')
    .controller('MainController', function ($scope, Principal,TodoList,TodoItem) {
        Principal.identity().then(function(account) {
//            $scope.openedListId = {item: 0};
            $scope.openedList = {todos:[]};
             $scope.todoLists = [];
             $scope.newItem = {};
             TodoList.query({page: $scope.page, size: 20}, function(result, headers) {

                             for (var i = 0; i < result.length; i++) {
                                 $scope.todoLists.push(result[i]);
                             }
                         });
            $scope.account = account;
            $scope.isAuthenticated = Principal.isAuthenticated;


             $scope.select = function(index,item){
                console.log(item);
                $scope.openedList = item;
                $scope.openedList.index = index;
                TodoList.get({id: item.id}, function(result) {
                        console.log(result);
                        $scope.openedList.todos = result.todoItems;
                        if(angular.isDefined(result.todoItems) == false){
                            $scope.openedList.todos = [];
                        }
                });
             }
             $scope.editTodo = function(item){
                 console.log(item , "update")
                TodoItem.update(item, function(result) {
                 console.log(result);
                });
             }
             $scope.editListSubject = function(){
                  console.log($scope.openedList , "update")
                  if(angular.isDefined($scope.openedList.id)){
                    TodoList.update($scope.openedList,
                    function(result) {
                           console.log(result);
                       });
                  }else{
                    TodoList.save($scope.openedList, function(result) {
                    result.todos = [];
                        $scope.todoLists.push(result);
                        $scope.openedList = result;

                           console.log(result);
                     });
                  }

              }
              $scope.newList = function(){
                   $scope.openedList={todos:[]};

                }
              $scope.newTodo = function(){
                   $scope.newItem.todoListId = $scope.openedList.id;
                       console.log($scope.newItem ,"new")

                   TodoItem.save($scope.newItem, function(result) {
                          console.log(result);
                          $scope.openedList.todos.push(result);
                          $scope.newItem = {};
                  });
              }
              $scope.deleteTodo = function(item,index){

                    TodoItem.delete({id:item.id}, function(result) {
                          console.log(result);
                          if(index != -1) {
                            $scope.openedList.todos.splice(index, 1);
                          }
                  });
              }
        });
    });
