'use strict';

angular.module('todoApp')
    .controller('TodoItemDetailController', function ($scope, $rootScope, $stateParams, entity, TodoItem, TodoList) {
        $scope.todoItem = entity;
        $scope.load = function (id) {
            TodoItem.get({id: id}, function(result) {
                $scope.todoItem = result;
            });
        };
        var unsubscribe = $rootScope.$on('todoApp:todoItemUpdate', function(event, result) {
            $scope.todoItem = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
