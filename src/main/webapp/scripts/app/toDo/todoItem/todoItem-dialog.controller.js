'use strict';

angular.module('todoApp').controller('TodoItemDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'TodoItem', 'TodoList',
        function($scope, $stateParams, $uibModalInstance, entity, TodoItem, TodoList) {

        $scope.todoItem = entity;
        $scope.todolists = TodoList.query();
        $scope.load = function(id) {
            TodoItem.get({id : id}, function(result) {
                $scope.todoItem = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('todoApp:todoItemUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.todoItem.id != null) {
                TodoItem.update($scope.todoItem, onSaveSuccess, onSaveError);
            } else {
                TodoItem.save($scope.todoItem, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.datePickerForDueDate = {};

        $scope.datePickerForDueDate.status = {
            opened: false
        };

        $scope.datePickerForDueDateOpen = function($event) {
            $scope.datePickerForDueDate.status.opened = true;
        };
}]);
