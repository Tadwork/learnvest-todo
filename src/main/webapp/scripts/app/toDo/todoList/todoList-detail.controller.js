'use strict';

angular.module('todoApp')
    .controller('TodoListDetailController', function ($scope, $rootScope, $stateParams, entity, TodoList, User, TodoItem) {
        $scope.todoList = entity;
        $scope.load = function (id) {
            TodoList.get({id: id}, function(result) {
                $scope.todoList = result;
            });
        };
        var unsubscribe = $rootScope.$on('todoApp:todoListUpdate', function(event, result) {
            $scope.todoList = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
