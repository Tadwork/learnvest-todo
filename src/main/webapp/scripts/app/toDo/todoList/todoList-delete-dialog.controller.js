'use strict';

angular.module('todoApp')
	.controller('TodoListDeleteController', function($scope, $uibModalInstance, entity, TodoList) {

        $scope.todoList = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            TodoList.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
