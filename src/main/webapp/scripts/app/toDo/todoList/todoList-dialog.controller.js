'use strict';

angular.module('todoApp').controller('TodoListDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'TodoList', 'User', 'TodoItem',
        function($scope, $stateParams, $uibModalInstance, entity, TodoList, User, TodoItem) {

        $scope.todoList = entity;
        $scope.users = User.query();
        $scope.todoitems = TodoItem.query();
        $scope.load = function(id) {
            TodoList.get({id : id}, function(result) {
                $scope.todoList = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('todoApp:todoListUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.todoList.id != null) {
                TodoList.update($scope.todoList, onSaveSuccess, onSaveError);
            } else {
                TodoList.save($scope.todoList, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
}]);
