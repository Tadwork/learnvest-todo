'use strict';

angular.module('todoApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('todoList', {
                parent: 'entity',
                url: '/todoLists',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'todoApp.todoList.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/todoList/todoLists.html',
                        controller: 'TodoListController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('todoList');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('todoList.detail', {
                parent: 'entity',
                url: '/todoList/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'todoApp.todoList.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/todoList/todoList-detail.html',
                        controller: 'TodoListDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('todoList');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'TodoList', function($stateParams, TodoList) {
                        return TodoList.get({id : $stateParams.id});
                    }]
                }
            })
            .state('todoList.new', {
                parent: 'todoList',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/todoList/todoList-dialog.html',
                        controller: 'TodoListDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    subject: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('todoList', null, { reload: true });
                    }, function() {
                        $state.go('todoList');
                    })
                }]
            })
            .state('todoList.edit', {
                parent: 'todoList',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/todoList/todoList-dialog.html',
                        controller: 'TodoListDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['TodoList', function(TodoList) {
                                return TodoList.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('todoList', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('todoList.delete', {
                parent: 'todoList',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/todoList/todoList-delete-dialog.html',
                        controller: 'TodoListDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['TodoList', function(TodoList) {
                                return TodoList.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('todoList', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
