'use strict';

angular.module('todoApp')
    .factory('TodoList', function ($resource, DateUtils) {
        return $resource('api/todoLists/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
                //isArray: true
            },
            'update': { method:'PUT' }
        });
    });
