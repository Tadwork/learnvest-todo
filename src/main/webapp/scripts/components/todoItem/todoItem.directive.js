'use strict';

angular.module('todoApp')
.directive('todoItem', function todoItem() {
  return {
    scope: {},
    restrict: 'E',
    bindToController: {
      item: '=',
      showDelete: '=',
      update : '&',
      deleteItem : '&'
    },
    controller: 'TodoItemController',
    controllerAs: 'todo',
//    templateUrl: 'scripts/components/todoItem/todoItem.html'
template:'<form class="form-inline">'+
             '<div class="checkbox">'+
               '<input type="checkbox" class="styled" ng-model="todo.item.completed" ng-change="todo.update(todo.item)">' +
               '<label></label>'+
             '</div>'+
             '<input type="text" style="width:80%" class="form-control" ng-model="todo.item.content" ng-change="todo.update(todo.item)"'+
                        'ng-model-options="{ updateOn: \'blur\' }" >'+
            '<button class="btn btn-danger glyphicon glyphicon-remove" ng-show="todo.showDelete" ng-click="todo.deleteItem(todo.item)"></button>'+
             /*'<div class="input-group">'+
                 '<input type="date" ng-change="todo.update(todo.item)" ng-model-options="{ updateOn: \'blur\' }" class="form-control" uib-datepicker-popup ng-model="todo.item.dueDate" is-open="todo.openedPopup" min-date="todo.minDate" datepicker-options="todo.dateOptions"  close-text="Close" />'+
                   '<span class="input-group-btn">'+
                     '<button type="button" class="btn btn-default" ng-click="todo.openCalendar()"><i class="glyphicon glyphicon-calendar"></i></button>'+
                   '</span>'+
             '</div>'+*/
         '</form>'
  };
});
