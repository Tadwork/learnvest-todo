package com.learnvest.todo.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.learnvest.todo.domain.TodoItem;
import com.learnvest.todo.domain.TodoList;
import com.learnvest.todo.repository.TodoItemRepository;
import com.learnvest.todo.repository.TodoListRepository;
import com.learnvest.todo.web.rest.util.HeaderUtil;
import com.learnvest.todo.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TodoItem.
 */
@RestController
@RequestMapping("/api")
public class TodoItemResource {

    private final Logger log = LoggerFactory.getLogger(TodoItemResource.class);

    @Inject
    private TodoItemRepository todoItemRepository;
    @Inject
    private TodoListRepository todoListRepository;
    /**
     * POST  /todoItems -> Create a new todoItem.
     */
    @Transactional
    @RequestMapping(value = "/todoItems",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TodoItem> createTodoItem(@RequestBody TodoItem todoItem) throws URISyntaxException {
        log.debug("REST request to save TodoItem : {}", todoItem);
        if (todoItem.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("todoItem", "idexists", "A new todoItem cannot already have an ID")).body(null);
        }
        TodoList list = todoListRepository.getOne(todoItem.getTodoListId());
        todoItem.setTodoList(list);
        TodoItem result = todoItemRepository.save(todoItem);

        return ResponseEntity.created(new URI("/api/todoItems/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("todoItem", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /todoItems -> Updates an existing todoItem.
     */
    @RequestMapping(value = "/todoItems",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TodoItem> updateTodoItem(@RequestBody TodoItem todoItem) throws URISyntaxException {
        log.debug("REST request to update TodoItem : {}", todoItem);
        if (todoItem.getId() == null) {
            return createTodoItem(todoItem);
        }
        TodoList list = todoListRepository.getOne(todoItem.getTodoListId());
        todoItem.setTodoList(list);
        TodoItem result = todoItemRepository.save(todoItem);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("todoItem", todoItem.getId().toString()))
            .body(result);
    }

   /* *//**
     * GET  /todoItems -> get all the todoItems.
     *//*
    @RequestMapping(value = "/todoItems",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<TodoItem>> getAllTodoItems(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of TodoItems");
        Page<TodoItem> page = todoItemRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/todoItems");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
*/
    /**
     * GET  /todoItems/:id -> get the "id" todoItem.
     */
    @RequestMapping(value = "/todoItems/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TodoItem> getTodoItem(@PathVariable Long id) {
        log.debug("REST request to get TodoItem : {}", id);
        TodoItem todoItem = todoItemRepository.findOne(id);
        return Optional.ofNullable(todoItem)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /todoItems/:id -> delete the "id" todoItem.
     */
    @RequestMapping(value = "/todoItems/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTodoItem(@PathVariable Long id) {
        log.debug("REST request to delete TodoItem : {}", id);
        todoItemRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("todoItem", id.toString())).build();
    }
}
