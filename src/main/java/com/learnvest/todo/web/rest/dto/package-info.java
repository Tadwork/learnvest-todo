/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.learnvest.todo.web.rest.dto;
