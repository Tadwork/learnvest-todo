package com.learnvest.todo.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.learnvest.todo.domain.TodoItem;
import com.learnvest.todo.domain.TodoList;
import com.learnvest.todo.repository.TodoListRepository;
import com.learnvest.todo.service.UserService;
import com.learnvest.todo.web.rest.util.HeaderUtil;
import com.learnvest.todo.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * REST controller for managing TodoList.
 */
@RestController
@RequestMapping("/api")
public class TodoListResource {

    private final Logger log = LoggerFactory.getLogger(TodoListResource.class);

    @Inject
    private TodoListRepository todoListRepository;

    @Inject
    private UserService user;
    /**
     * POST  /todoLists -> Create a new todoList.
     */
    @RequestMapping(value = "/todoLists",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TodoList> createTodoList(@RequestBody TodoList todoList) throws URISyntaxException {
        log.debug("REST request to save TodoList : {}", todoList);
        if (todoList.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("todoList", "idexists", "A new todoList cannot already have an ID")).body(null);
        }

        todoList.setUser(user.getUserWithAuthorities());
        TodoList result = todoListRepository.save(todoList);
        return ResponseEntity.created(new URI("/api/todoLists/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("todoList", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /todoLists -> Updates an existing todoList.
     */
    @RequestMapping(value = "/todoLists",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TodoList> updateTodoList(@RequestBody TodoList todoList) throws URISyntaxException {
        log.debug("REST request to update TodoList : {}", todoList);
        if (todoList.getId() == null) {
            return createTodoList(todoList);
        }

        TodoList result = todoListRepository.save(todoList);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("todoList", todoList.getId().toString()))
            .body(result);
    }

    /**
     * GET  /todoLists -> get all the todoLists.
     */
    @RequestMapping(value = "/todoLists",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<TodoList>> getAllTodoLists(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of TodoLists");
        Page<TodoList> page = todoListRepository.findByUserIsCurrentUser(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/todoLists");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
//    /**
//     * GET  /todoLists -> get all the todoLists.
//     */
//    @RequestMapping(value = "/todoLists",
//        method = RequestMethod.GET,
//        produces = MediaType.APPLICATION_JSON_VALUE)
//    @Timed
//    public ResponseEntity<List<TodoList>> getAllTodoListsForUser()
//        throws URISyntaxException {
//        log.debug("REST request to get a TodoLists for logged in user");
//        List<TodoList> page = todoListRepository.findByUserIsCurrentUser();
//        return new ResponseEntity<>(page,  HttpStatus.OK);
//    }
    /**
     * GET  /todoLists/:id -> get the "id" todoList.
     */
    @RequestMapping(value = "/todoLists/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TodoList> getTodoList(@PathVariable Long id) {
        log.debug("REST request to get TodoList : {}", id);
        TodoList todoList = todoListRepository.findOneByIdAndUserWithTodoItems(id);
        return Optional.ofNullable(todoList)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /todoLists/:id -> delete the "id" todoList.
     */
    @RequestMapping(value = "/todoLists/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTodoList(@PathVariable Long id) {
        log.debug("REST request to delete TodoList : {}", id);
        todoListRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("todoList", id.toString())).build();
    }
}
