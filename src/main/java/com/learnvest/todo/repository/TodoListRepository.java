package com.learnvest.todo.repository;

import com.learnvest.todo.domain.TodoList;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data JPA repository for the TodoList entity.
 */
public interface TodoListRepository extends JpaRepository<TodoList,Long> {

    @Query("select todoList from TodoList todoList where todoList.user.login = ?#{principal.username}")
    Page<TodoList> findByUserIsCurrentUser(Pageable page);
    @Query("select todoList from TodoList todoList JOIN FETCH todoList.todoItems where todoList.user.login = ?#{principal.username} AND  todoList.id = (:id)")
    TodoList findOneByIdAndUserWithTodoItems(@Param("id") Long id);
}
