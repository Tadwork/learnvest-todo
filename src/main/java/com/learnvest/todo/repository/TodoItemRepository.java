package com.learnvest.todo.repository;

import com.learnvest.todo.domain.TodoItem;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the TodoItem entity.
 */
public interface TodoItemRepository extends JpaRepository<TodoItem,Long> {

}
